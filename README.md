## Quickstart
Clone the repository to a local location and setup the environment:

    $ git clone git@gitlab.com:jerrykan/jerrykan.gitlab.io.git
    $ cd jerrykan.gitlab.io
    $ git submodule init
    $ git submodule update
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    $ pelican -lr

Browse to http://127.0.0.1:8000/

## Description
This is the source repository of my personal blog. Commits pushed to this
repository will trigger a rebuild of the website using the
[Pelican](https://getpelican.com) static site generator.


## Usage
It is assumed that [Git](https://git-scm.com/) and a recent version of [Python
3](https://www.python.org/downloads/) are installed on the system.

Detailed information on using Pelican can be found in the comprehensive
[Pelican documentation](http://docs.getpelican.com/en/stable/).

### Setup the virtualenv
The virtualenv can be setup and activated with the required Python packages by
running the following commands in the directory containing the local clone of
the repository:

    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

The virtualenv can be deactivated at any time by running:

    $ deactivate

The following sections assume the virtualenv is active.

### Generate website file
With the virtualenv correctly setup and activated a local copy of the website
can be built by running:

    $ pelican

The output of the statically generate website will be available in the
`output/` directory.

### View website
The locally generated website can be viewed by running a local HTTP server
provided by Pelican:

    $ pelican -l

Browse to http://127.0.0.1:8000/ to view the website.

### Watch for changes
Pelican can watch for changes to any files and automatically regenerate the
website:

    $ pelican -r

This can be combined with the `-l`/`--listen` option to automatically serve up
the newly regenerated website as changes are made to the source files:

    $ pelican -lr

Browse to http://127.0.0.1:8000/ and reload the page after new changes are made
and the pages regenerated.

### Adding a post
New posts are added by creating a new file in the `content/` directory with a
filename using the format `YYYY-MM-DD-<post title slug>`:

    vi content/2018-11-20-new-post.md

#### Markdown posts
Posts made using the Markdown markup language should include metadata at the
top of the file in the following format:

    Title: New Post
    Category: General
    Tags: tag1, tag2

    This is the content on the new post. You can use markdown to markup the
    contents and make it more visually appealing.

#### reStructuredText posts
Posts made using the reStructuredText markup language should include metadata
at the top of the file in the following format:

    New Post
    ========

    :category: General
    :tags: tag1, tag2

    This is the content on the new post. You can use markdown to markup the
    contents and make it more visually appealing.

### Publish updates
Updates to website are published to the live website by committing and pushing
the changes to the GitLab repository:

    $ git add content/2018-11-20-new-post.md
    $ git commit
    $ git push origin master

The website is automatically regenerated and deployed using the [GitLab
CI/Pages](https://docs.gitlab.com/ee/user/project/pages/) feature. This process
can take a few minutes to complete and progress can be tracked by looking at
the [Pipelines](https://gitlab.com/taslug/taslug.gitlab.io/pipelines) page.


## Also see:
  - [Pelican documentation](http://docs.getpelican.com/en/stable/content.html)
    - detailed information about writing content for Pelican based sites.
  - [Python-Markdown documentation](https://python-markdown.github.io/)
    - information about the specific Markdown implementation used by Pelican
      and the available non-standard extensions that can be used.
  - [reStructuredText documentation](http://docutils.sourceforge.net/rst.html)
