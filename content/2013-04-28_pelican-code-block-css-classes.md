Title: Pelican Code Block CSS Classes
Category: General
Tags: pelican

[Pelican] provides support for generating content from two markup languages -
[reStructuredText] (the default) and [Markdown] (if installed). For the most
part both markup languages generate similar output, except when using
[Pygments] to generate code blocks with syntax highlighting.

Code blocks generated from reStructuredText will use the CSS class `highlight`
to handle the syntax highlighting, while Markdown will use the `codehilite`
class by default. This can cause problems when developing themes for Pelican
users who may be using either reStructuredText or Markdown, or users who choose
to generate content using both markup languages.

Fortunately you can customise how the Markdown processor generates its output
using the `MD_EXTENSIONS` setting in the `pelicanconf.py` file. You can
configure the Markdown processor to generate code blocks using the `highlight`
CSS class by inserting the following entry in you `pelicanconf.py` file:

    :::
    MD_EXTENSIONS = ['codehilite(css_class=highlight)']

More information about pelicanconf.py settings can be found in the Pelican
[documentation].


[pelican]: http://docs.getpelican.com/
[restructuredtext]:
[markdown]: http://daringfireball.net/projects/markdown/
[pygments]: http://pygments.org/
[documentation]: http://docs.getpelican.com/en/latest/settings.html#basic-settings
