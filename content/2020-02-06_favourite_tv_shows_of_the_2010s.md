Title: Favourite TV Shows of the 2010s
Category: General
Tags: tv

![Legion]({static}/images/legion.jpg)

**Eligibility:** Only shows that have aired more than one season can qualify,
and at least one season must have aired in the 2010s

1. _[Legion](https://www.imdb.com/title/tt5114356/)_ (2017-2019)
2. _[BoJack Horseman](https://www.imdb.com/title/tt3398228/)_ (2014-2020)
3. _[Halt and Catch Fire](https://www.imdb.com/title/tt2543312/)_ (2014-2017)
4. _[Mr Robot](https://www.imdb.com/title/tt4158110/)_ (2015-2019)
5. _[The Magicians](https://www.imdb.com/title/tt4254242/)_ (2015- )
6. _[Rick and Morty](https://www.imdb.com/title/tt2861424/)_ (2013- )
7. _[The Expanse](https://www.imdb.com/title/tt3230854/)_ (2015- )
8. _[The Good Place](https://www.imdb.com/title/tt4955642/)_ (2016-2020)
9. _[Community](https://www.imdb.com/title/tt1439629/)_ (2009-2015)
10. _[Justified](https://www.imdb.com/title/tt1489428/)_ (2010-2015)
11. _[Counterpart](https://www.imdb.com/title/tt4643084/)_ (2017-2019)
12. _[Fleabag](https://www.imdb.com/title/tt5687612/)_ (2016-2019)
13. _[12 Monkeys](https://www.imdb.com/title/tt3148266/)_ (2015-2018)
14. _[Bluey](https://www.imdb.com/title/tt7678620/)_ (2018- )
15. _[Mad Men](https://www.imdb.com/title/tt0804503/)_ (2007-2015)
16. _[True Detective](https://www.imdb.com/title/tt2356777/)_ (2014- )
17. _[Silicon Valley](https://www.imdb.com/title/tt2575988/)_ (2014-2019)
18. _[30 Rock](https://www.imdb.com/title/tt0496424/)_ (2006-2013)
19. _[Avatar: The Legend of Korra](https://www.imdb.com/title/tt0499549/)_ (2012-2014)
20. _[End of the F**cking World](https://www.imdb.com/title/tt6257970/)_ (2017-2019)

If there appear to be any obvious omissions, then I probably haven't found time
to watch it yet... or I just thought it was overrated.
