(function(fn) {
  if (document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
})(function() {
  var prefix = document.getElementById('prefix');
  var suffix = document.getElementById('suffix');
  var affixes = document.querySelectorAll('.affix');

  document.querySelectorAll('#links a').forEach(function(item) {
    item.addEventListener('mouseenter', function() {
      affixes.forEach(function(affix) {
        affix.style.opacity = 0;
      });
      prefix.textContent = item.dataset.prefix || '';
      prefix.style.opacity = 1;
      suffix.textContent = item.dataset.suffix || '';
      suffix.style.opacity = 1;

      // HUH?
      links_width = document.getElementById('links').clientWidth;
      jerrykan = prefix.parentElement.nextElementSibling;
      limit = (links_width - jerrykan.clientWidth) / 2;

      if (prefix.clientWidth > limit || suffix.clientWidth > limit) {
        translate = (prefix.clientWidth - suffix.clientWidth) / 2;
      };
    });
    item.addEventListener('mouseleave', function() {
      affixes.forEach(function(affix) {
        affix.style.opacity = 1;
      });
      prefix.style.opacity = 0;
      suffix.style.opacity = 0;
    });
  });
});
